import sys
from random import *
from math import *
from time import clock
from copy import *
import numpy

# seed(15156468)
# seed(1)

# liczba maszyn m = 2
# p. wyjściowy instacji n=50

# Zakres czasowy
min_czas = 5
max_czas = 20

# Inicjalizacja przerwy jako zm. globalnej
Przerwa = 0
# Ilosc zadan
# podawać ilość +1
Ilosc_Zadan = 51
Ilosc_Mrowek = 100
Wspolczynnik_Parowania = 0.2

Ilosc_Feromonow = 40

Iteracje = 40

# do testow
Czas_optymalny = 0
Czas_poczatkowy = 0

Zadania_Tab = []

TEMP_parM1 = []
TEMP_Przerwy = []
TEMP_parM2 = []

FIN_parM1 = []
FIN_Prerwy = []
FIN_parM2 = []

FIN_UK = []


class ZadanieClass:

    def __init__(self, numer: int):
        self.ID_zadania = numer
        self.op1 = randrange(min_czas, max_czas)
        self.op2 = randrange(min_czas, max_czas)
        self.czy_przerwa = 0

    def __str__(self):
        return str(self.op1) + ";" + str(self.op2) + "\n"

    def __repr__(self):
        return str(tuple((self.ID_zadania, self.czy_przerwa)))

    def ustalenie(self, M):
        # 1 gdy z Przerwa, 0 bez
        self.czy_przerwa = M


# pojedyncza mrówka bedzie dostarcvzac rozwiacanie
class Mrowka:

    def __init__(self, ile_zadan):
        self.liczba_zadan = ile_zadan
        # obecne_zadanie - Pierwsze zadanie, dalej obecne_zadanie zadanie, przydałoby się losowac jakoś
        self.obecne_zadanie = (0, 0)
        self.wynik = []
        self.lista_mozliwosci = [(i, a) for a in range(2) for i in range(1, ile_zadan)]
        # self.korekta_mozliwosci()
        # print(self.lista_mozliwosci)

    # chyba zmienie tab. pokrycia, przesune o jeden
    def rozwiazanie(self, tablica_pokrycia):
        for i in range(self.liczba_zadan - 1):
            self.przygotwanie_ruletki(tablica_pokrycia)
            self.wybor()
        return self.wynik

    # Przygotowanie tablicy do wylosowania nastepnego zadania, obecne zadanie znane
    def przygotwanie_ruletki(self, tablica_pokrycia):
        # lista_mozliwosci bedzie zawierac możliwe zadania
        # wagi, bedzie zawierrać odpowaidające "wagi" tego zadania
        self.wagi = []
        for i in range(len(self.lista_mozliwosci)):
            if self.lista_mozliwosci[i][1] == 0:

                if self.obecne_zadanie[1] == 0:
                    self.wagi.append(tablica_pokrycia[self.obecne_zadanie[0]][self.lista_mozliwosci[i][0]])
                else:
                    self.wagi.append(
                        tablica_pokrycia[self.obecne_zadanie[0] + Ilosc_Zadan][self.lista_mozliwosci[i][0]])
            else:
                if self.obecne_zadanie[1] == 0:
                    self.wagi.append(
                        tablica_pokrycia[self.obecne_zadanie[0]][self.lista_mozliwosci[i][0] + Ilosc_Zadan])
                else:
                    self.wagi.append(tablica_pokrycia[self.obecne_zadanie[0] + Ilosc_Zadan][
                                         self.lista_mozliwosci[i][0] + Ilosc_Zadan])

    def wybor(self):

        self.obecne_zadanie = choices(self.lista_mozliwosci, weights=self.wagi, k=1)[0]
        self.wynik.append(self.obecne_zadanie)
        self.korekta_mozliwosci()

    def korekta_mozliwosci(self):
        self.lista_mozliwosci.remove((self.obecne_zadanie[0], 0))
        self.lista_mozliwosci.remove((self.obecne_zadanie[0], 1))



# Przejście z X na Y, X->Y [x][y]
def tablica_pokrycia_pocztkowa(liczba_zadan):
    lista = [[1 for x in range(liczba_zadan * 2)] for y in range(liczba_zadan * 2)]
    for x in range(0, liczba_zadan):
        lista[x][x] = 0
        lista[x][x + liczba_zadan] = 0
        lista[x + liczba_zadan][x] = 0
        lista[x + liczba_zadan][x + liczba_zadan] = 0
    return lista


# zwraca liste obiektów zadan i zmienia zmienna globalna "Przerwa"
def generator_instancji():
    suma = 0
    zadania = []
    zadania2 = []
    temp = ZadanieClass(0)
    temp.op1 = 0
    temp.op2 = 0
    zadania.append(temp)
    zadania2.append(deepcopy(zadania[0]))
    for i in range(1, Ilosc_Zadan):
        zadania.append(ZadanieClass(i))
        zadania2.append(deepcopy(zadania[i]))
        zadania2[i].ustalenie(1)
    for i in range(len(zadania)):
        suma += zadania[i].op1

    suma = suma // (Ilosc_Zadan - 1)
    global Przerwa
    Przerwa = randrange(suma, max_czas)
    zadania = zadania + zadania2

    return zadania


# maszyna 1, zzwraca czas
def tworzenie_uszeregowania_1(rozwiazanie, rozwiazanie2):
    kara = 1
    maszyna1_Czasy = []
    counter = 0
    global TEMP_parM1
    global TEMP_Przerwy

    TEMP_TEMP = []
    TEMP_TEMP_P = []
    for i in range(Ilosc_Zadan - 1):
        tmp = [rozwiazanie[i][0], counter, Zadania_Tab[rozwiazanie[i][0]].op1,
               ceil(Zadania_Tab[rozwiazanie[i][0]].op1 * kara)]
        counter += tmp[2]
        TEMP_TEMP.append(tmp)

        if rozwiazanie[i][1]:
            kara = 1
            if i < (Ilosc_Zadan - 1):
                TEMP_TEMP_P.append([counter, Przerwa])
                counter += Przerwa
            else:
                kara += 0.1
        maszyna1_Czasy.append(counter)
    TEMP_parM1.append(TEMP_TEMP)
    TEMP_Przerwy.append(TEMP_TEMP_P)
    return tworzenie_uszeregowania_2(rozwiazanie, maszyna1_Czasy, rozwiazanie2)

    # zapisywanie zadanie -> czas zakonczenia, uwzgledniajac przerwy ale ich nie zapisujac
    # później liczenie dla 2 maszyny
    #  Mam liste zadan. Sumuje czasy zakończeń, przemnozon przez kare  z tego sufit) i kolejno wypisuje odpowiednio operacje
    # Nastepniee dla drugiej czas zakończenia 1 zadania+ czas trwania 2.
    # Jeśli 2 na m1 sie skończyło to wykonuje drugie i sumuje bez kary, tak jak na 1 maszynie
    # czyli jesli t1[i]<=t2[i-1] -> t2[i] = t2[i-1]+tm2


def tworzenie_uszeregowania_2(maszyna1, maszyna1_Czasy, maszyna2):
    maszyna2_Czasy = []
    tmp = []
    global TEMP_parM2
    TEMP_TEMP = []
    for i in range(Ilosc_Zadan - 1):
        if maszyna2[0][0] == maszyna1[i][0]:
            maszyna2_Czasy.append(maszyna1_Czasy[i] + Zadania_Tab[maszyna2[0][0]].op2)
            tmp = [maszyna2[0][0], maszyna1_Czasy[i], Zadania_Tab[maszyna2[0][0]].op2]
            TEMP_TEMP.append(tmp)
            break
    for i in range(1, Ilosc_Zadan - 1):
        tmp = []
        for j in range(Ilosc_Zadan - 1):
            if maszyna2[i][0] == maszyna1[j][0]:
                if maszyna1_Czasy[j] > maszyna2_Czasy[i - 1]:
                    maszyna2_Czasy.append(maszyna1_Czasy[j] + Zadania_Tab[maszyna1[i][0]].op2)
                    tmp = [maszyna2[i][0], maszyna1_Czasy[j], Zadania_Tab[maszyna1[i][0]].op2]
                else:
                    maszyna2_Czasy.append(maszyna2_Czasy[i - 1] + Zadania_Tab[maszyna1[i][0]].op2)
                    tmp = [maszyna2[i][0], maszyna2_Czasy[i], Zadania_Tab[maszyna1[i][0]].op2]
                TEMP_TEMP.append(tmp)
                break

    # Ostatni
    TEMP_parM2.append(TEMP_TEMP)
    return maszyna2_Czasy[-1]


def najmniejsze(a, N):
    return numpy.argsort(a)[::1][:N]


def parowanie(tablica, wspolczynnik):
    for i in range(2 * Ilosc_Zadan):
        for j in range(2 * Ilosc_Zadan):
            tablica[i][j] = tablica[i][j] * wspolczynnik


def modyfikacja_tabli(tablica_Pokrycia, wynik):
    obecne = (0, 0)
    for i in range(Ilosc_Zadan - 1):
        if obecne[1] == 0:
            if wynik[i][1] == 0:
                tablica_Pokrycia[obecne[0]][wynik[i][0]] += Ilosc_Feromonow
            else:
                tablica_Pokrycia[obecne[0]][wynik[i][0] + Ilosc_Zadan] += Ilosc_Feromonow
        else:
            if wynik[i][1] == 0:
                tablica_Pokrycia[obecne[0] + Ilosc_Zadan][wynik[i][0]] += Ilosc_Feromonow
            else:
                tablica_Pokrycia[obecne[0] + Ilosc_Zadan][wynik[i][0] + Ilosc_Zadan] += Ilosc_Feromonow
        obecne = wynik[i]


def program():
    # Dla 2 maszny dodatkowa tablica bedzie potrzeban
    tablica_Pokrycia = tablica_pokrycia_pocztkowa(Ilosc_Zadan)
    tablica_Pokrycia_2 = tablica_pokrycia_pocztkowa(Ilosc_Zadan)
    temp = 999999999

    global TEMP_parM1
    global TEMP_Przerwy
    global TEMP_parM2

    global FIN_parM2
    global FIN_parM1
    global FIN_Prerwy
    global Czas_poczatkowy
    global FIN_UK
    TEMP_parM2 = []
    TEMP_parM1 = []
    TEMP_Przerwy = []
    FIN_UK = []
    for k in range(Iteracje):
        tablica_Mrowek = []
        tablica_Mrowek2 = []
        wyniki = []
        wyniki2 = []
        czasy = []
        for i in range(Ilosc_Mrowek):
            tablica_Mrowek.append(Mrowka(Ilosc_Zadan))
            tablica_Mrowek2.append(Mrowka(Ilosc_Zadan))
            wyniki.append(tablica_Mrowek[i].rozwiazanie(tablica_Pokrycia))
            wyniki2.append(tablica_Mrowek2[i].rozwiazanie(tablica_Pokrycia_2))
            czasy.append(tworzenie_uszeregowania_1(wyniki[i], wyniki2[i]))

        # 10 najlepszych rozwiazan
        best = list(najmniejsze(czasy, 10))
        for j in best:
            modyfikacja_tabli(tablica_Pokrycia, wyniki[j])
            modyfikacja_tabli(tablica_Pokrycia_2, wyniki2[j])

        # Parowanie
        if k % 2 == 0:
            parowanie(tablica_Pokrycia, Wspolczynnik_Parowania)
            parowanie(tablica_Pokrycia_2, Wspolczynnik_Parowania)

        if temp > czasy[best[0]]:
            temp = czasy[best[0]]
            FIN_parM1 = TEMP_parM1[best[0]]
            FIN_Prerwy = TEMP_Przerwy[best[0]]
            FIN_parM2 = TEMP_parM2[best[0]]
            FIN_UK = wyniki[best[0]]

        if k == 0:
            Czas_poczatkowy = czasy[best[0]]

    global Czas_optymalny
    Czas_optymalny = temp


def zliczanie_przerw(w1):
    counter = 0
    for i in range(len(w1)):
        if w1[i][1] == 1:
            counter += 1
    return counter


def zapis_instacncji(miejsce, numer):
    global TEMP_parM1
    global TEMP_Przerwy
    global TEMP_parM2

    global Zadania_Tab
    temp = str(miejsce + "/instancja_" + str(numer)) + ".txt"
    plik = open(temp, 'w')
    plik.write("****" + str(numer) + "****\n")
    plik.write(str(Ilosc_Zadan - 1) + "\n")
    for z in range(1, Ilosc_Zadan):
        plik.write(str(Zadania_Tab[z]))
    plik.write(str(Przerwa) + "\n")
    plik.write("****EOF****\n")
    plik.write("Ilosc Feromow;" + str(Ilosc_Feromonow) + "\n")
    plik.write("Wspołczynnik Parowania;" + str(Wspolczynnik_Parowania) + "\n")
    plik.close()


# SZABLON
# FIN M1/M2 [ID,t0,czas(,czas realny)]
# FIN Przerw [t0,czas]
# FIN _ l Przerw
def zapis_wynikow(miejsce, numer):
    global FIN_parM1
    global FIN_Prerwy
    temp = str(miejsce + "/wynik_" + str(numer)) + ".txt"
    plik = open(temp, 'w')
    plik.write("****" + str(numer) + "****\n")
    plik.write(str(Czas_optymalny) + ";" + str(Czas_poczatkowy) + "\n")
    ile_przerw = zliczanie_przerw(FIN_UK)
    plik.write("M1:")
    for q in FIN_UK:
        tmp = FIN_parM1.pop(0)
        plik.write("op2_" + str(tmp[0]) + "," + str(tmp[1]) + "," + str(tmp[2]) + ";")
        if q[1] == 1 and len(FIN_Prerwy) != 0:
            tmp = FIN_Prerwy.pop(0)
            plik.write("maint1," + str(tmp[0]) + "," + str(tmp[1]))

    plik.write("\nM2:")
    for i in FIN_parM2:
        plik.write("op2_" + str(i[0]) + "," + str(i[1]) + "," + str(i[2]) + ";")
    plik.write("\n")
    plik.write(str(ile_przerw) + "," + str(Przerwa * ile_przerw))
    plik.write("\n****EOF****\n")
    plik.close()


def main():
    global Zadania_Tab

    global Ilosc_Zadan
    global max_czas
    global min_czas
    global Ilosc_Feromonow
    global Iteracje
    global Wspolczynnik_Parowania
    tab_w = []
    tmp = 1
    for i in range(20):
        tab_w.append(tmp)
        tmp -= 0.05

    # tab_F = [5, 10, 15, 20, 25, 30, 35, 40, 50, 100]
    tab_F = [45, 55, 60, 65, 70, 75, 80, 85, 90, 95]

    # tab_I = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60,80, 100]
    tab_I = [200, 300]

    # Parametry wejściowe F15,W0.8,I25
    """
    # Testy feromonow, startowa 15
    print("Feromony")
    for i in range(len(tab_F)):
        print(i)
        print(clock())
        miejsce = "testy_F"
        Ilosc_Feromonow = tab_F[i]
        for j in range(10):
            numer = i * 10 + j
            Zadania_Tab = generator_instancji()
            zapis_instacncji(miejsce, numer)
            program()
            zapis_wynikow(miejsce,numer)
   
    Ilosc_Feromonow = 15
     """
    """
    print("Współczynnik")
    for j in range(len(tab_w)):
        print(j)
        print(clock())
        miejsce = "testy_W"
        Wspolczynnik_Parowania = tab_w[j]
        for i in range(10):
            numer = j * 10 + i
            Zadania_Tab = generator_instancji()
            zapis_instacncji(miejsce, numer)
            program()
            zapis_wynikow(miejsce,numer)
    
    Wspolczynnik_Parowania = 0.8
    """
    """
    print("Iteracje")
    for j in range(len(tab_I)):
        print(j)
        print(clock())
        miejsce = "testy_I"
        Iteracje = tab_I[j]
        for i in range(10):
            numer = j * 10 + i
            Zadania_Tab = generator_instancji()
            zapis_instacncji(miejsce, numer)
            program()
            zapis_wynikow(miejsce, numer)

    Iteracje = 25
    """
    # Iteracje 40

    """
    tab_IZ = [11, 21, 31, 41, 51, 61, 71, 81, 91, 101]
    print("Ilosc zadan")
    for j in range(len(tab_IZ)):
        print(j)
        print(clock())
        miejsce = "testy_IZ"
        Ilosc_Zadan = tab_IZ[j]
        for i in range(10):
            numer = j * 10 + i
            Zadania_Tab = generator_instancji()
            zapis_instacncji(miejsce, numer)
            program()
            zapis_wynikow(miejsce, numer)
    
    Ilosc_Zadan = 51
    """
    """
    print("Dużo krótkich/Kilka Długich")
    for j in range(len(tab_I)):
        print(j)
        print(clock())
        miejsce = "testy_I"
        # Iteracje = tab_I[j]
        for i in range(10):
            numer = j * 10 + i
            Zadania_Tab = generator_instancji()
            zapis_instacncji(miejsce, numer)
            program()
            zapis_wynikow(miejsce, numer)
    """
    """
    tab_R = [150,200]
    print("Coraz większa rozpiętość czasus")
    for j in range(len(tab_R)):
        print(j)
        print(clock())
        miejsce = "testy_R,200"
        max_czas = min_czas + tab_R[j]
        for i in range(10):
            numer = j * 10 + i
            Zadania_Tab = generator_instancji()
            zapis_instacncji(miejsce, numer)
            program()
            zapis_wynikow(miejsce, numer)

    print(clock())
    """

    for i in range(10000000):
        pass

main()
